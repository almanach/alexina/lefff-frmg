#!/usr/bin/env perl -w

use CGI::Pretty qw/-compile -nosticky :standard *pre *table *div map :html3 -private_tempfiles -oldstyle_urls/;
use CGI::Carp qw(fatalsToBrowser);
use IPC::Run  qw(start finish run pump);

use strict;


##$ENV{PATH} = '/bin:/usr/bin/:/usr/local/bin';

local our $query = new CGI::Pretty;

######################################################################
## Configuration

our $PACKAGE = '@results10@';
our $htmldir = '/var/www/perl/$PACKAGE';

our $h;
our $in;
our $out;

init();

######################################################################
## Reading parameters

our ($search) = $query->param(-name=>'search');

emit_form();

######################################################################
## Emitting Data

sub emit_form {
  print 
    header,
      start_html( 
		 -title => 'Querying Lefff',
		 -style => { -src => 'style.css' }
		),
		  h1('Querying Lefff');
  print  start_form;
  print  p('Search',
	   $query->textfield(-name=>'search',
			     -size=>30,
			     -default=> ''
			    ));
  print end_form;
  print hr;
  if (defined $search) {
    print get_lefff($search);
    print hr;
  }
  print end_html;
}


sub init {
  return if (defined $h);
  my @lexed = 
    ('/usr/bin/lexed',
     '-d','/usr/local/lib/lefff-frmg',
     '-p','dico.xlfg',
     'consult');
  $h = start \@lexed,
    \$in,\$out;

}

sub get_lefff {
  my $lex = shift;
  $in .= "$lex\n";
  ##  close(IN);
  $h->pump until $out;
  my $info = $out;
  $out = '';
  $info =~ s/^\d+//g;
  $info =~ s/\|\d*(\s+)/\n|$1/g;
  return  pre(escapeHTML($info));
}

=head1 NAME

lefff.pl -- CGI script to search Lefff

=head1 DESCRIPTION

Copy this file to /var/www/perl/.

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2007, INRIA.

This program is free software; you can redistribute it and/or modify 
it under the same terms as Perl itself.

=head1 AUTHOR

=over 4

=item Eric de la Clergerie, <Eric.De_La_Clergerie@inria.fr>

=back

=head1 SEE ALSO

=over 4

=item * Lefff L<http://alpage.inria.fr/catalogue.fr.html#Lefff>

=item * Online demo L<http://alpage.inria.fr/lefffdemo>

=cut
