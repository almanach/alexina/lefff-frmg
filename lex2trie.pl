#!/usr/bin/env perl

# build a trie from lefff entries

use strict;

use Text::Scan;

my $output = shift;

my $dict = new Text::Scan;

my $entries = {};

while (<>) {
  chomp;
  my ($form,$data) = split(/\t/,$_,2);
  push(@{$entries->{$form}},$data);
}

foreach my $form (keys %$entries) {
  $dict->insert($form, join('|||',@{$entries->{$form}}));
}

$dict->serialize($output);
