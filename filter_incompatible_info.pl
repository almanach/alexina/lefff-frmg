#!/usr/bin/env perl

$fs = qr/(?:Suj|Obj�|Objde|Obj|Loc|Dloc|Obl\d?|Att)/;

while (<>) {
  chomp;
  while (s/($fs:\(?[^,]*?)(et|�_l'avis_de|d'avec|jusqu'�|tps)-[^\|,\)>]+/\1/g) {}
#  while (s/($fs:\(?[^,]*?)(et|�_l'avis_de|d'avec|du_c�t�_de|selon|suivant|au_travers_de|au_d�pens_de|le_long_de|depuis|face_�|au_sujet_de|�_travers|derri�re|chez|envers|au|jusque|jusqu'�|en_faveur_de|entre|tps)-[^\|,\)>]+/\1/g) {}
  while (s/($fs:\(?(?:[^,]*\|)?)_/\1/g) {}
  while (s/($fs:\(?[^,]*)_-/\1-/g) {}
  while (s/($fs:\(?)\|+/\1/g) {}
  while (s/($fs:\(?[^\|,\)>]+)\|\|+/\1\|/g) {}
  s/\|\|+/\|/g;
  s/($fs:\(?[^,\)>]+?)\|+([\),>])/\1\2/g;
  s/\|se(r[e�]fl|r[�e]c)//g;
  s/loc-source/loc/gi;
  s/Loc!de/loc/gi;
  s/de_sa-sn/de-sn/gi;
  s/op�r-sn/comme-sn|pour-sn/gi;
  s/de-source!de/loc/gi;
  s/([\(\|])\(([^\(\)]+)\)/$1\2/gi;
  s/\(([^\(\)]+)\)([\)\|])/\1\2/gi;
  s/($fs:(?:\(\))?),//g;
  s/,($fs:(?:\(\))?)>/>/;
  s/,$fs:\(\(.*?([,>])/\1/g;
  print "$_\n";
}
