#!/usr/bin/env perl
# $Id$

while(<>) {
    chomp;
    s/[^\"]\#.*$//;
    s/^\#.*$//;
    s/\@e *,//;
    s/\\//;
    s/,* *\@e *\]/\]/;
    s/^ +//;
    if ($_!~/^[ \t]*$/) {
	/^([^\t]+)\t(.*)$/;
	$c1=$1;
	$c2=$2;
	$c1=~s/ *\' */\'/g;
	$c1=~s/ *\- */\-/g;
	$c1=~s/ +/ /g;
	$c2=~s/__det//g;
	$c2=~s/__prep//g;
	$c2=~s/\\\'/_/g;

	# EVDLC
	# handle ncpred to add missing subjects
	# should be handled by benoit in lefff !
# 	if ($c2 =~ /ncpred/) {
# 	  ## ncpred with non-empty frame
# 	  $c2 =~ s/pred=\'(.+?)<(.+?)>(.*?)\'/pred='$1<subj,$2>$3'/g;
# 	  ## ncpred with empty frame
# 	  $c2 =~ s/pred=\'([^<>\']+?)\'/pred='$1<subj>'/g;
# 	}

	## EVLDC
	# remove comma as lemma virgule<arg1,arg2>
	next if /pred="__virgule<arg1,arg2>"/;

	## EVLDC
	# remove comma as lemma virgule<arg1,arg2>
	next if /pred="__virgule<arg1,arg2>"/;

	## EVDLC 
	# remove est-ce que as pri
	# next if /est-ce_que/;

	## EVDLC
	## deep2surface transformation on passive until frmg updated to handle
	## deep syntactic/thematic roles
# 	if (/\@passive/ && $c2 =~ /par-sn/) {
# 	  $c2 =~ s/<obj:([^,]+)(.*?),suj:\(par-sn\)(.*?)>/<obj:(par-sn),suj:$1$2$3>/o;
# 	  $c2 =~ s/<Obj:([^,]+)(.*?),Suj:\(par-sn\)(.*?)>/<Obj:(par-sn),Suj:$1$2$3>/o;
# 	}

	# lefff #svn > 883
	if (/PastParticiple/ && /\@passive/) {
	  $c2 =~ s/<(.+?),(Obl2:[^>,]+?)>/<$2,$1>/o;
	}

	# lefff #svn > 994: pb with some synt_head= that have no value
	next if $c2 =~ /synt_head=[,\]]/;

	# lefff #svn > ??? : pb with modnc= +
	$c2 =~ s/modnc\s*=\s*\+/adv_kind=modnc/og;

	# lefff #svn > ??? : preciser aux_req pour participle active pronominal
	if (/PastParticiple/ && $c2 =~ /\@active/ && $c2  =~ /\@pron/) {
	  $c2 =~ s/(\@active)/$1,\@�tre/;
	}

	# lefff #svn > ??? : preciser aux_req pour participle active
	if (/PastParticiple/ && $c2 =~ /\@active/ && $c2  !~ /\@�tre/) {
	  $c2 =~ s/(\@active)/$1,\@avoir/;
	}

	# deal with ',,' for advm (bug in Lefff)
	$c2 =~ s/,,/,/og;

	# deal_with_quotes
#	$c2=~s/pred\s*=\s*\'([^\[]*)\'((?: *,[^>]*)? *\])( *\;.*)$/pred=\"\1\"\2\3/g;
#	$c2=~s/=\[pred\s*=\s*\'([^\']*)\' *([\,\]])/=\[pred=\"\1\"\2/g;
	$c2=~s/\\//g;
	while ($c2=~s/(\"[^ \"\[]*) ([^\[]*\")/\1_\2/) {};

	# new lefff use " as delimiters for lemma subcat info
	# but still some errors
	$c2 =~ s/pred='(.*?)'/pred="$1"/og;

	$c2 =~ s/pred="(.+?)<>"/pred="$1"/og;

	$c2 =~ s/pred="(.+?)<>"/pred="$1"/og;

	$c2 =~ s/pred="\(se\) /pred="se /og;
	$c2 =~ s/pred="\(s'\)/pred="s'/og;
	$c2 =~ s/pred="se_\(se\)/pred="se_/og;
	$c2 =~ s/pred="s'\(s'\)/pred="s'/og;

	## seems to be a better semantic for that pair of macro
	$c2 =~ s/\@pron,\@�tre_possible/\@pron_possible/o;

	## for lglex
	$c2 =~ s/lightverb=se\s+faire/lightverb=faire/o;

	print "$c1\t$c2\n";
    }
}
